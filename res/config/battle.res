t Tplayer res/textures/game/player.png
t Teagle res/textures/game/eagle.png
t Tbomber res/textures/game/volcanus.png
t Tobstacle res/textures/game/meteor.png
t Tbullet res/textures/game/bullet.png
t Tglow res/textures/game/glow.png
t Tshield res/textures/game/shield.png
t Trocket res/textures/game/sfm18.png
s player Tplayer 0 0 50 50 0 0
s eagle Teagle 0 0 40 40 0 0
s bomber Tbomber 0 0 60 60 0 0
s obstacle Tobstacle 0 0 50 50 0 0
s bulletGreen Tbullet 0 0 20 20 1 0 255 0 255 0
s glowGreen Tglow 0 0 50 50 1 0 255 0 150 0
s shieldCyan Tshield 0 0 70 70 1 50 100 255 255 0
s rocket Trocket 0 0 40 10 0 0
f default res/fonts/pixelated.ttf